const todoInput = document.querySelector(".todo-input");
const todoBtn = document.querySelector(".todo-btn");
const todoList = document.querySelector(".todo-list");
    
function addTodo(event){
    event.preventDefault();
    const todoDiv = document.createElement('div');
    const newTodo = document.createElement('li');
    newTodo.innerText= todoInput.value;
    todoDiv.classList.add('todo');
    const compBtn = document.createElement('button');
    compBtn.innerHTML = '<i class="fa fa-check" aria-hidden="true"></i>';
    compBtn.classList.add('comp-btn');
    const delBtn = document.createElement('button');
    delBtn.innerHTML = '<i class="fa fa-trash" aria-hidden="true"></i>';
    delBtn.classList.add('trash-btn')
    todoDiv.appendChild(newTodo);
    todoDiv.appendChild(compBtn);
    todoDiv.appendChild(delBtn);
    todoList.appendChild(todoDiv);
    todoInput.value = "";
}
todoBtn.addEventListener('click', addTodo);

todoList.addEventListener('click', deleteCheck);
function deleteCheck(e){ 
    let item = e.target;
    if(item.classList[0]=== "trash-btn"){
       const todo = item.parentElement;
       todo.remove();
    }
    else if(item.classList[0]=== "comp-btn"){
        const todo = item.parentElement;
        todo.classList.toggle("completed");
    }
}

