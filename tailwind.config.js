module.exports = {
  purge: [],
  theme: {
    extend: {},
  },
  variants: {
    backgroundColor: ['responsive', 'hover', 'focus', 'active', 'checked'],
    textColor: ['responsive', 'hover', 'focus', 'checked', 'visited'],
  },
  plugins: [require('@tailwindcss/custom-forms')],
}
